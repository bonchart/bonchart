import Message from '../models/Message'
import handlebarsModules from '../modules/HandleBars';
import helpers from '../modules/helpers';

const {
  BuildTemplate,
} = handlebarsModules;

const {
} = helpers;

module.exports = {
  send: (req, res) => {
    let message = {
        text: req.body.messageText,
        date: new Date(),
        from: req.session.nickname,
        to: req.body.messageTo
    }
    new Message(message).save((err) => {
        return err ? res.sendStatus(500) : res.sendStatus(200);
    })

  },
  getChat: (req, res) => {
    Message.find().or([
        {from: req.session.nickname, to: req.body.chat},
        {from: req.body.chat, to: req.session.nickname}
    ])
    .sort([['date', -1]])
    .then(messages => { res.send(messages) })
    .catch(error => { res.sendStatus(500) })
  }
};
