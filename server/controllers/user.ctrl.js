import languages from 'language-list';
import country from 'countryjs';
import User from '../models/User';
import handlebarsModules from '../modules/HandleBars';

const {
  BuildTemplate,
} = handlebarsModules;

module.exports = {
  accountsLogin: (req, res) => {
    const {
      nickname,
      password,
    } = req.body;

    User.authenticate(nickname, password, (err, user) => {
      if (user) {
        req.session.userId = user.id;
        req.session.nickname = user.nickname;

        return res.redirect('/welcome');
      }
      return res.redirect('/');
    });
  },
  accountsLogout: (req, res) => {
    if (req.session) {
      // delete session object
      req.session.destroy(err => err || res.redirect('/'));
    }
  },
  registerUser: (req, res) => {
    const {
      addressCountry,
      addressProvince,
    } = req.body;

    req.body.nickname = req.body.nickname.toLowerCase();

    req.body.address = {
      country: addressCountry,
      province: addressProvince,
    };

    new User(req.body).save((err) => {
      if (err) {
        const {
          errors,
        } = err;
        const Page = BuildTemplate('01-signup');
        const languageList = languages().getData();

        return res.send(
          Page({
            errors,
            languageList,
            values: req.body,
            countryList: country.all(),
          }),
        );
      }
      return res.redirect('/welcome');
    });
  },
  showSignupPage: (req, res) => {
    const Page = BuildTemplate('01-signup');
    const languageList = languages().getData();
    res.send(
      Page({
        languageList,
        countryList: country.all(),
      }),
    );
  },
  searchUsers: (req, res) => {
    const {
      search,
    } = req.body;

    User.find({
      nickname: new RegExp(search, 'i'),
    }).limit(5).exec((err, users) => {
      let data = [];

      if (users) {
        data = users.map(user => ({
          image: user.profileImage || '/img/author-main1.jpg',
          nickname: user.nickname,
          name: `${user.firstName} ${user.lastName}`,
        }));
      }
      res.send({
        data,
      });
    });
  },
};
