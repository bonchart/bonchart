import UserPost from '../models/UserPost'
import handlebarsModules from '../modules/HandleBars';
import middleWare from '../modules/MiddleWares';
import helpers from '../modules/helpers';

const {
  BuildTemplate,
} = handlebarsModules;

const {} = helpers;

module.exports = {
  makePost: (req, res) => {
    let userPost = {
      text: req.body.postText,
      date: new Date(),
      author: req.session.userId
    }
    new UserPost(userPost).save( (err) => {
      if (err) {
        res.sendStatus(500)
      } else {
        const Page = BuildTemplate('../partials/posts/posts1-BP');
        res.send(Page({post:userPost}));
      }
    });
  },
  makeComment: (req, res) => {
    let comment = {
      text: req.body.commentText,
      date: new Date(),
      author: req.session.userId
    }
    UserPost.findById(req.body.postId, (err, post) => {
      if (err) {
        res.sendStatus(500);
      } else {
        post.comments.push(comment);
        post.save((err) => {
          return err ? res.sendStatus(500) : res.sendStatus(200);
        })
      }
    })

  },
};