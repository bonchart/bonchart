import handlebarsModules from '../modules/HandleBars';
import helpers from '../modules/helpers';

const {
  BuildTemplate,
} = handlebarsModules;

const {
  getUserData,
  showErrorPage,
} = helpers;

module.exports = {
  showHomePage: (req, res) => {
    const Page = BuildTemplate('00-home');
    const isNotLogged = !req.session.userId;
    res.send(Page({
      isNotLogged,
    }));
  },
  showErrorPage: (req, res) => {
    res.send(showErrorPage(req));
  },
  showFavPages: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('16-FavPagesFeed');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
  showLandingPage: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('03-Newsfeed');

      res.send(
        Page({
          user,
        }),
      );
    });
  },
  showSearchPage: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('03-Newsfeed-Masonry');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
};
