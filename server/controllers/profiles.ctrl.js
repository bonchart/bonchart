import User from '../models/User';
import UserPost from '../models/UserPost'
import handlebarsModules from '../modules/HandleBars';
import helpers from '../modules/helpers';

const {
  BuildTemplate,
} = handlebarsModules;

const {
  getUserData,
  showErrorPage,
  errorFallBack,
} = helpers;
const findUserPost = async function (authorId) {
  return await UserPost.find({ author: authorId }).sort([['date', -1]])
}

const showWithUserData = (req, res, Page, params) => {
  const {
    params: {
      nickname,
    } = {},
  } = req;

  User.findOne({
    nickname: nickname.toLowerCase(),
  })
    .exec((err, userData) => {
      if (err || !userData) {
        res.send(showErrorPage(req));
      } else {
        getUserData(req.session.userId, (user) => {
          findUserPost(userData.id).then(posts => {
            const pageParams = Object.assign({}, {
              userRequested: userData,
              userPosts: posts,
              user,
            },
              params);
              console.log(posts)
            res.send(Page(pageParams));
          });
        });
      }
    });
};

module.exports = {
  showProfile: (req, res) => {
    const Page = BuildTemplate('02-ProfilePage');
    showWithUserData(req, res, Page, {});
  },
  redirectProfile: (req, res) => {
    res.redirect(`/profile/${req.session.nickname}`);
  },
  showEvents: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('20-CalendarAndEvents-MonthlyCalendar');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
  showFriendsBirthdays: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('25-FriendsBirthday');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
  showFriendsGroups: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('17-FriendGroups');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
  friendRequestAccept: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const friend = user.friendRequest.find(({
        nickname,
      }) => nickname === req.params.nickname);

      if (friend) {
        User.findOneAndUpdate({
          nickname: user.nickname,
        }, {
            $pull: {
              friendRequest: friend.id,
            },
          }, {
            new: true,
          },
          errorFallBack);

        User.findOneAndUpdate({
          nickname: user.nickname,
        }, {
            $addToSet: {
              friendList: friend.id,
            },
          }, {
            new: true,
          },
          errorFallBack);

        User.findOneAndUpdate({
          nickname: friend.nickname,
        }, {
            $addToSet: {
              friendList: user.id,
            },
          }, {
            new: true,
          },
          errorFallBack);
      }
      res.sendStatus(200);
    });
  },
  friendRequestDeny: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const friend = user.friendRequest.find(({
        nickname,
      }) => nickname === req.params.nickname);

      if (friend) {
        User.findOneAndUpdate({
          nickname: user.nickname,
        }, {
            $pull: {
              friendRequest: friend.id,
            },
          }, {
            new: true,
          },
          errorFallBack);
      }

      res.sendStatus(200);
    });
  },
  showStatistics: (req, res) => {
    getUserData(req.session.userId, (user) => {
      const Page = BuildTemplate('26-Statistics');
      res.send(
        Page({
          user,
        }),
      );
    });
  },
  friendRequest: (req, res) => {
    const {
      nickname,
    } = req.params;
    const {
      userId,
    } = req.session;

    User.findOneAndUpdate({
      nickname,
    }, {
        $addToSet: {
          friendRequest: userId,
        },
      }, {
        new: true,
      },
      errorFallBack);
    res.sendStatus(200);
  },
  showProfileAbout: (req, res) => {
    const Page = BuildTemplate('05-ProfilePage-About');

    showWithUserData(req, res, Page, {});
  },
  showFriends: (req, res) => {
    const Page = BuildTemplate('06-ProfilePage');
    console.log()

    showWithUserData(req, res, Page, {});
  },
  showPhotos: (req, res) => {
    const Page = BuildTemplate('07-ProfilePage-Photos');

    showWithUserData(req, res, Page, {});
  },
  showVideos: (req, res) => {
    const Page = BuildTemplate('09-ProfilePage-Videos');

    showWithUserData(req, res, Page, {});
  },
};
