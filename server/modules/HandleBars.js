import handlebars from 'handlebars';
import path from 'path';
import fs from 'fs';

const dir = (path.join(__dirname, '../../src/partials/'));
const isWin = process.platform === "win32";

const walkSync = (dir, filelist = []) => {
  fs.readdirSync(dir).forEach(file => {
    filelist = fs.statSync(path.join(dir, file)).isDirectory() ?
      walkSync(path.join(dir, file), filelist) :
      filelist.concat(path.join(dir, file));
  });
  return filelist;
};

const filelist = walkSync(dir);
if (filelist.length > 0) {
  filelist.forEach(function (filename) {
    const file = isWin ? filename.replace(/\\/g, "/") : filename;
    console.log(file)
    const matches = /partials\/([^.]+).hbs/.exec(file);
  
    if (!matches) {
      return;
    }

    const name = matches[1];
    const template = fs.readFileSync(file, 'utf8');
    handlebars.registerPartial(name, template);
  });
  console.log(`=======- ${filelist.length} partials registered. -=======`);
}

handlebars.registerHelper('orOperator', function (value, secondValue) {
  return value || secondValue;
});

handlebars.registerHelper('isSelected', function (value, secondValue) {
  return value === secondValue ? 'selected' : '';
});

handlebars.registerHelper('isEqual', function (a, b, options) {
  if (a == b) {
    return options.fn(this);
  }
});

handlebars.registerHelper('isNotEqual', function (a, b, options) {
  if (a != b) {
    return options.fn(this);
  }
});

handlebars.registerHelper('defaultImage', function (image) {
  return image ? image : '/img/author-main1.jpg';
});

handlebars.registerHelper('isIn', function (value, list, options) {
  if (!!list.find(elem => elem == value)) {
    return options.fn(this);
  }
});

handlebars.registerHelper('isNotIn', function (value, list, options) {
  if (!list.find(elem => elem == value)) {
    return options.fn(this);
  }
});

handlebars.registerHelper('dateFormat', function (value = '') {
  return new Date(value).toLocaleDateString("en-US", { year: 'numeric', month: 'long', day: 'numeric' });
});

handlebars.registerHelper('noFriends', function (user, userRequested, options) {
  const noWaitingForApprove = !userRequested.friendRequest.find(elem => elem == user.id);
  const noPendingRequest = !user.friendRequest.find(({ nickname }) => nickname == userRequested.nickname);
  const noInFriendList = !userRequested.friendList.find(elem => elem == user.id)

  if (noInFriendList && noWaitingForApprove && noPendingRequest) {
    return options.fn(this);
  }
});

handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
  switch (operator) {
    case '==':
      return v1 == v2 ? options.fn(this) : options.inverse(this);
    case '===':
      return v1 === v2 ? options.fn(this) : options.inverse(this);
    case '!=':
      return v1 != v2 ? options.fn(this) : options.inverse(this);
    case '!==':
      return v1 !== v2 ? options.fn(this) : options.inverse(this);
    case '<':
      return v1 < v2 ? options.fn(this) : options.inverse(this);
    case '<=':
      return v1 <= v2 ? options.fn(this) : options.inverse(this);
    case '>':
      return v1 > v2 ? options.fn(this) : options.inverse(this);
    case '>=':
      return v1 >= v2 ? options.fn(this) : options.inverse(this);
    case '&&':
      return v1 && v2 ? options.fn(this) : options.inverse(this);
    case '||':
      return v1 || v2 ? options.fn(this) : options.inverse(this);
    default:
      return options.inverse(this);
  }
});

module.exports = {
  BuildTemplate: (page, res) => {
    const source = fs.readFileSync(`src/pages-sources/${page}.hbs`, 'utf-8');
    return handlebars.compile(source);
  }
};