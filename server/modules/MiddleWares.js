const requiresLogin = (req, res, next) => {
  if (req.session && req.session.userId) {
    return next();
  }
  const err = new Error('You must be logged in to view this page.');
  err.status = 401;
  return res.redirect('/');
};

const withoutLogin = (req, res, next) => {
  if (req.session && req.session.userId) {
    return res.redirect('/welcome');
  }
  return next();
};

module.exports = {
  requiresLogin,
  withoutLogin,
};
