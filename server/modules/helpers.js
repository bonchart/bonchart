import User from '../models/User';
import handlebarsModules from './HandleBars';

const {
  BuildTemplate,
} = handlebarsModules;

const getUserData = (id, next) => {
  User.findOne({
    _id: id,
  })
    .populate('friendRequest')
    .populate('friendList')
    .exec((err, user) => {
      let userData = {};

      if (user) {
        const friendRequest = user.friendRequest;
        
        userData = {
          id: user.id,
          nickname: user.nickname,
          lastName: user.lastName,
          firstName: user.firstName,
          friendList: user.friendList,
          friendRequest,
        };
      }
      next(userData);
    });
};

const showErrorPage = (req) => {
  const Page = BuildTemplate('65-OlympusCompanyPage404Error');
  const isNotLogged = !req.session.userId;

  return Page({
    isNotLogged,
  });
};

const errorFallBack = (err, doc) => {
  if (err) {
    console.log('Something wrong when updating data!');
  }

  console.log(doc);
};

module.exports = {
  getUserData,
  showErrorPage,
  errorFallBack,
};
