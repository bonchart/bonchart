import mongoose from 'mongoose';

const PostSchema = new mongoose.Schema(
  {
    text: String,
    title: String,
    description: String,
    post_img: String,
    post_video: String,
    likes: Number,
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    comments: [
      {
        author: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User',
        },
        text: String,
      },
    ],
  },
);
/* eslint-disable func-names */
PostSchema.methods.like = function () {
  this.likes += 1;
  return this.save();
};

PostSchema.methods.comment = function (c) {
  this.comments.push(c);
  return this.save();
};

PostSchema.methods.addAuthor = function (authorId) {
  this.author = authorId;
  return this.save();
};

/* eslint-enable func-names */

module.exports = mongoose.model('Post', PostSchema);
