import mongoose from 'mongoose';

const MessageSchema = new mongoose.Schema(
  {
    text: String,
    date: Date,
    likes: Number,
    from: String,
    to: String
  },
);

/* eslint-enable func-names */
module.exports = mongoose.model('Message', MessageSchema);
