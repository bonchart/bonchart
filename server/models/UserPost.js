import mongoose from 'mongoose';

const UserPostSchema = new mongoose.Schema(
  {
    text: { type: String, required: true },
    post_img: String,
    likes: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }],
    date: { type: Date, required: true },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    comments: [
      {
        author: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User',
        },
        text: String,
        date: Date
      },
    ],
  },
);
/* eslint-disable func-names */
UserPostSchema.methods.like = function () {
  this.likes += 1;
  return this.save();
};

UserPostSchema.methods.comment = function (c) {
  this.comments.push(c);
  return this.save();
};

UserPostSchema.methods.addAuthor = function (authorId) {
  this.author = authorId;
  return this.save();
};

/* eslint-enable func-names */

module.exports = mongoose.model('UserPost', UserPostSchema);
