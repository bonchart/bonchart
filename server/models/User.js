import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import bcrypt from 'bcrypt-nodejs';

const UserSchema = new mongoose.Schema({
  password: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  nickname: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  gender: { type: String, required: true },
  language: { type: String, required: true },
  phoneNumber: { type: String },
  address: {
    country: String,
    province: String,
  },
  birthdate: { type: Date, required: true },
  bornCountry: { type: String, required: true },
  artisticPreferences: [
    {
      art: String,
      derivatives: [{ name: String }],
    },
  ],
  token: String,
  profileImage: String,
  followers: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  following: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  friendList: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  friendRequest: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  // profile info
  aboutMe: { type: String },
  occupation: { type: String },
  website: { type: String },
  hobbies: { type: String },
  favorites: {
    series: { type: String },
    movies: { type: String },
    games: { type: String },
    music: { type: String },
    writers: { type: String },
    books: { type: String },
  },
  otherInterest: { type: String },
  educationEmployment: [
    {
      title: { type: String },
      fromYear: { type: String },
      toYear: { type: String },
      description: { type: String },
    },
  ],
});

/* eslint-disable func-names */
UserSchema.methods.follow = function (userId) {
  if (this.following.indexOf(userId) === -1) {
    this.following.push(userId);
  }
  return this.save();
};

UserSchema.methods.addFollower = function (fs) {
  this.followers.push(fs);
};

// hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
  const user = this || {};
  const salt = bcrypt.genSaltSync(10);
  bcrypt.hash(user.password, salt, null, (err, hash) => {
    if (err) {
      return next(err);
    }
    user.password = hash;
    return next();
  });
});

// authenticate input against database
UserSchema.statics.authenticate = function (nick, password, callback) {
  const nickname = nick.toLowerCase();

  mongoose
    .model('User', UserSchema)
    .findOne({ nickname })
    .exec((err, user) => {
      if (err) {
        let error;

        if (!user) {
          error = new Error('User not found.');
          error.status = 401;
        }

        return callback(!user ? error : err);
      }

      bcrypt.compare(password, user.password, (err, result) => {
        if (result === true) {
          return callback(null, user);
        }
        return callback();
      });
    });
};

UserSchema.plugin(uniqueValidator, { message: 'This {PATH} is already in use.' });

/* eslint-enable func-names */

module.exports = mongoose.model('User', UserSchema);
