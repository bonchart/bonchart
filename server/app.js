/* eslint-disable no-console */

import express from 'express';
import session from 'express-session';
import mongoose from 'mongoose';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import routes from './routes';

/*Local db*/
const DB = 'mongodb://localhost:27017/bonchart';
const MongoDBStore = require('connect-mongodb-session')(session);

const app = express();
const store = new MongoDBStore({
  uri: DB,
  collection: 'mySessions',
});
const assert = require('assert');

// Catch errors
store.on('error', (error) => {
  assert.ifError(error);
  assert.ok(false);
});

app.use(session({
  secret: '-[-sUpErr SeeCRet PassWORD-]-',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
  },
  store,
}));

const router = express.Router();
const url = process.env.MONGODB_URI || DB;

try {
  mongoose.connect(url, { useNewUrlParser: true, useCreateIndex: true });
} catch (error) {
  console.log('Sorry, the connection with mongo was unable to success.');
}

const port = process.env.PORT || 5000;

/** set up routes {API Endpoints} */
routes(router);

/** set up middlewares */
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(helmet());
// app.use('/static',express.static(path.join(__dirname,'static')))

app.use('/', router); // Set url entrypoint, ex: "/api/{{routes}}"
app.use(express.static('public'));
/** start server */
app.listen(port, () => {
  console.log(`Server started at port: ${port}`);
});
/* eslint-enable no-console */
