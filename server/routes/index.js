import user from './user';
import profiles from './profiles';
import main from './main';

module.exports = (router) => {
  user(router);
  profiles(router);
  main(router);
};
