import { requiresLogin } from '../modules/MiddleWares';
import postsController from '../controllers/posts.ctrl';

module.exports = (router) => {
  router.post('/add/post', requiresLogin, postsController.makePost);
};
