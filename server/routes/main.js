import path from 'path';
import fs from 'fs';
import mainController from '../controllers/main.ctrl';

import { requiresLogin, withoutLogin } from '../modules/MiddleWares';

const dir = path.join(__dirname, '../../public/');

const getDirectories = route => fs.readdirSync(route).filter(file => fs.statSync(`${route}/${file}`).isDirectory());
const publicFolders = getDirectories(dir).join('|');

module.exports = (router) => {
  router.get('/', withoutLogin, mainController.showHomePage);
  router.get('/pages', requiresLogin, mainController.showFavPages);
  router.get('/welcome', requiresLogin, mainController.showLandingPage);
  router.route(new RegExp(`^((?!/(${publicFolders})).)*$`)).get(mainController.showErrorPage);
};
