import userController from '../controllers/user.ctrl';

module.exports = (router) => {
  router.route('/accounts/login').post(userController.accountsLogin);
  router.route('/accounts/signup').get(userController.showSignupPage);
  router.route('/accounts/signup').post(userController.registerUser);
  router.route('/accounts/logout').get(userController.accountsLogout);
  router.route('/search/users').post(userController.searchUsers);
};
