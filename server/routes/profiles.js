import profilesController from '../controllers/profiles.ctrl';
import postsController from '../controllers/posts.ctrl';
import messageController from '../controllers/message.ctrl';

import {
  requiresLogin,
} from '../modules/MiddleWares';

module.exports = (router) => {
  router.get('/profile', requiresLogin, profilesController.redirectProfile);
  router.get('/profile/:nickname', requiresLogin, profilesController.showProfile);
  router.post('/profile/:nickname/friend/request', requiresLogin, profilesController.friendRequest);
  router.post('/profile/:nickname/friend/request/accept', requiresLogin, profilesController.friendRequestAccept);
  router.post('/profile/:nickname/friend/request/deny', requiresLogin, profilesController.friendRequestDeny);
  router.get('/profile/:nickname/events', requiresLogin, profilesController.showEvents);
  router.get('/profile/:nickname/about', requiresLogin, profilesController.showProfileAbout);
  router.get('/profile/:nickname/friends', requiresLogin, profilesController.showFriends);
  router.get('/profile/:nickname/photos', requiresLogin, profilesController.showPhotos);
  router.get('/profile/:nickname/videos', requiresLogin, profilesController.showVideos);
  router.get('/profile/:nickname/statistics', requiresLogin, profilesController.showStatistics);
  router.get('/profile/:nickname/friends/groups', requiresLogin, profilesController.showFriendsGroups);
  router.get('/profile/:nickname/friends/birthdays', requiresLogin, profilesController.showFriendsBirthdays);

  router.post('/add/post', requiresLogin, postsController.makePost);
  router.post('/add/comment', requiresLogin, postsController.makeComment);
  router.post('/message/send', requiresLogin, messageController.send);
  router.post('/message/messages', requiresLogin, messageController.getChat);
};
