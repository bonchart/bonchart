$(document).ready(function () {
    const $provinces = $('#provinceList option').detach(); // global variable
    updateChildsByAttr('provinceList', $provinces, 'country', 'AF');

    $('#countryList').on('change', function (e) {
        $("#provinceList option").remove();
        updateChildsByAttr('provinceList', $provinces, 'country', e.target.value);
    });

    function updateChildsByAttr(id, childs, attr, value) {
        $(`#${id}`).empty().append(childs.filter(`[${attr}=${value}]`) );
    }
});
