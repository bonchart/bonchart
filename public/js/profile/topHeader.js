$(document).ready(() => {
  $('#sendFriendRequest').click(function() {
    var nickname = $(this).data('user');
    $(this).remove();

    fetch(`/profile/${nickname}/friend/request`, {
      method: 'POST'
    });
  });

  $('.accept-request:not(.request-del)').click(function() {
    var nickname = $(this).data('user');
    $(this).closest('li').fadeOut( 'fast', function() {
      var number = Number($('#friendRequestCount').text()) -1
      if (number === 0) {
        $('#friendRequestCount').remove();
        return;
      }
      $('#friendRequestCount').text(number)
      $(this).remove();
    });

    fetch(`/profile/${nickname}/friend/request/accept`, {
      method: 'POST'
    });
  });

  $('.request-del').click(function() {
    var nickname = $(this).data('user');
    $(this).closest('li').fadeOut( 'fast', function() {
      var number = Number($('#friendRequestCount').text()) -1
      $('#friendRequestCount').text(number)
      $(this).remove();
    });

    fetch(`/profile/${nickname}/friend/request/deny`, {
      method: 'POST'
    });
  });
});
