$(document).ready(function () {
    // FilePond.parse(document.body);
    // First register any plugins
    $.fn.filepond.registerPlugin(FilePondPluginImagePreview);

    // Turn input element into a pond
    $('.filepond').filepond();

    // Listen for addfile event
    $('.filepond').on('FilePond:addfile', function(e) {
        console.log('file added event', e);
    });
});